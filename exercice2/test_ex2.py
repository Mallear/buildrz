from .Classes import Rectangle, Carre

def test_rectangle_aire():
  r = Rectangle(2, 3)
  assert r.aire() == 6
  del(r)

def test_rectangle_perimetre():
  r = Rectangle(2, 3)
  assert r.perimetre() == 10
  del(r)

def test_rectangle_compte():
  r1 = Rectangle(2, 3)
  r2 = Rectangle(2, 3)
  r3 = Rectangle(2, 3)
  assert r1.compte() == 3
  del(r1)
  del(r2)
  del(r3)

####

def test_carre_aire():
  c = Carre(2)
  assert c.aire() == 4
  del(c)

def test_carre_perimetre():
  c = Carre(2)
  assert c.perimetre() == 8
  del(c)

def test_carre_compte():
  c1 = Carre(2)
  c2 = Carre(2)
  c3 = Carre(2)
  assert c1.compte() == 3
  del(c1)
  del(c2)
  del(c3)

####

def test_rectangle_carre_compte():
  c1 = Carre(2)
  c2 = Carre(2)
  c3 = Carre(2)
  r1 = Rectangle(2, 3)
  r2 = Rectangle(2, 3)
  r3 = Rectangle(2, 3)
  assert c1.compte() == 6
  assert r1.compte() == 6
