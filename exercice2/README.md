## 2 - Python

Les classes se trouvent dans le fichier `Classes.py` et les tests sont présents dans `test_ex2.py`.

```bash
# From the repository root
poetry install
poetry shell
pytest
```
