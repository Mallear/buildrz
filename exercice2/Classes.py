
class Rectangle(object):

  _instance_number = 0

  def __init__(self, largeur, longueur):
    self.largeur = largeur
    self.longueur = longueur
    Rectangle._instance_number += 1

  def aire(self):
    return self.largeur * self.longueur

  def perimetre(self):
    return (self.largeur * 2) + (self.longueur * 2)

  def compte(self):
    return Rectangle._instance_number

  def __del__(self):
    Rectangle._instance_number -= 1


class Carre(Rectangle):
  def __init__(self, largeur):
    Rectangle.__init__(self, largeur,largeur)