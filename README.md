# Assessment

- [Exercice 1](./exercice1/README.md)
- [Exercice 2](./exercice2/README.md)
- Exercice 3 - Not found
- [Exercice 4](./exercice4.md)
- [Exercice 5](./exercice5.md)

Pour lancer les tests des exercices 1 et 2:

```bash
# From the repository root
poetry install
poetry shell
pytest
```
