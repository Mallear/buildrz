## 4 - Data Ops

---

- Ce script se connecte en SSH sur un serveur et execute un dump d'une base PostgreSQL. Et ce sur plusieurs tables à la suite.

---

- Ce script execute les dumps séquentiellement, ce qui peut va être de plus en plus long si les tables se remplissent. De plus, si la taille des dumps est trop grande, cela peut poser problème si la configuration du serveur concernant la taille maximum d'un fichier est trop petite. Le dump créé n'est présent que sur la machine, il faudrait l'uploader sur un stockage externe (stockage objet par exemple) pour éviter de le perdre si le serveur tombe.

---

- Pour améliorer le script, on peut rediriger la sortie de la commande `pgdump` dans `gzip` pour compresser directement le fichier dump et éviter une limitation de taille sur le serveur.
