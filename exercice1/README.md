## 1 - Maximum Différence

La fonction et ses tests sont présents dans `test_ex1.py`.

```bash
# From the repository root
poetry install
poetry shell
pytest
```
