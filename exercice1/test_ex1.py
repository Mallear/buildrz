def max_difference(t):
  current = 0

  for i in t:
    if abs(i) > abs(current):
      current = i
    elif abs(i) == abs(current):
      current = abs(current)

  return current

def test_all_positive():
  assert max_difference([1,2,3,4]) == 4

def test_all_negative():
  assert max_difference([-11,-2,-3,-4]) == -11

def test_negative_and_positive():
  assert max_difference([-1,2,-3,4]) == 4

def test_negative_and_positive_egal():
  assert max_difference([-4,2,-3,4]) == 4

def test_empty_array():
  assert max_difference([]) == 0

#####

def max_difference_rec(t, maximum=0):
  if len(t)==0:
    return maximum
  elif abs(t[0]) > abs(maximum):
    return max_difference_rec(t[1:], t[0])
  elif abs(t[0]) == abs(maximum):
    return max_difference_rec(t[1:], abs(maximum))


def test_all_positive_rec():
  assert max_difference_rec([1,2,3,4]) == 4

def test_all_negative_rec():
  assert max_difference_rec([-11,-2,-3,-4]) == -11

def test_negative_and_positive_rec():
  assert max_difference_rec([-1,2,-3,4]) == 4

def test_negative_and_positive_egal_rec():
  assert max_difference_rec([-4,2,-3,4]) == 4

def test_empty_array_rec():
  assert max_difference_rec([]) == 0
