## 5 - Docker / Kubernetes

---

- Une image avec un poid trop important peut impacter le volume de l'instance sur laquelle le container est déployé lors que plusieurs versions de l'image sont présentent sur l'instance.

---

- Il y a plusieurs possibilités pour réduire la taille d'une image:

  - Utiliser une image de base "lite". Ici on remplacerait l'image "python" par une image "python:slim" ( ou "python:3.8-slim-buster" pour spécifier la version de python et de l'OS sousjascent)
  - Désactiver et supprimer le cache des packages managers applicatif ou système (ici apt et pip).
  - Utiliser le multi-stage build pour ne garder que le nécessaire dans l'image finale.

  Dans cette image, on pourrait aussi créer un utilisateur applicatif pour éviter de lancer l'application avec le user root pour plus de sécurité.

---

- Les bonnes pratiques de développement applicatif consistent à définir ce genre d'informations dans des variables d'environnements ou bien à aller les chercher dans une solution type parameter store/vault.
  Dans le cas des variables d'environnements, Kubernetes permet de gérer des `secrets` stockés dans le composant etcd du cluster. Ces secrets sont simplement encodés en base64 mais la base etcd peut (**doit**) être chiffré. En utilisant un outil tel que [external-secret](https://github.com/external-secrets/kubernetes-external-secrets), la définition de ces secrets peut même être versionné sans risquer de leak puisque les valeurs réelles sont stocké dans une solution comme AWS Parameter Store, Hashicorp Vault, Azure Key vault etc.

  Dans le second cas, l'application peut s'occuper d'aller chercher les informations dans une solution telle que AWS Parameter Store/Secret Manager ou bien Hashicorp Vault pour n'en citer que deux. Cette solution à l'avantage que les valeurs ne sont pas accessible via l'environnement dans lequel tourne l'application mais implique une plus forte dépendance à la solution dans laquelle sont stockées les valeurs.

---

- Pour cela, on peut d'abord lancer un job kubernetes (construit sur la même image Docker) qui sera responsable des actions en base avant le déploiement de la nouvelle version applicative.

---

- Il manque un objet Ingress permettant d'exposer le service sur l'extérieur.

---

- Sur EKS, l'ingress controller [AWS Load Balancer Controller](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.3/) permet de créer et configurer des ALB directement à partir de la définition des ingress dans le cluster. On pourrait donc définir une ingress comme suit:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
    name: my-app
    namespace: my-app
    annotations:
        kubernetes.io/ingress.class: alb
        alb.ingress.kubernetes.io/group.name: my-app
        alb.ingress.kubernetes.io/scheme: internet-facing
        alb.ingress.kubernetes.io/target-type: ip
spec:
    rules:
    - host: my-app.example.com
        http:
        paths:
          - path: /
            pathType: ImplementationSpecific
            backend:
              service:
                name: my-app
                port:
                  number: 80
```

Cela permettrait la création d'un ALB côté AWS avec un target group et un listener configurés pour que les requêtes arrivant sur `my-app.example.com` soit redirigées vers le service `my-app`.

---

- Pour limiter un surcout important lié aux VM Windows, il est possible de provisionner à la demande les instances nécessaires. Un outil comme [Karpenter](https://karpenter.sh/) permet d'ajouter des noeuds dans un cluster EKS avec une configuration prédéfinie (taille de l'instance, OS, architecture processeur ...). En utilisant des instances Spot combinées à la fonctionnalité de Karpenter qui supprime les noeuds inutilisées à la fin d'une période définie, les surcoûts peuvent être limités.
